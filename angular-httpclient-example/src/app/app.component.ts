import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { DataService, GetFlightsPayloadData } from './data-service/data-service';
import { NgdbDropdownBasicComponent } from './home/dropdown/ngdb-dropdown-basic/ngdb-dropdown-basic.component';
import { NgbdDatepickerBasicComponent } from './home/ngbd-datepicker-basic/ngbd-datepicker-basic.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChildren('cityPicker') cityPickerComponents: QueryList<NgdbDropdownBasicComponent>;
  @ViewChildren('datePicker') datePickerComponents: QueryList<NgbdDatepickerBasicComponent>

  flights: any
  flightsBack: any

  submit(e: any) {
    e.preventDefault();
  }

  constructor(private dataService: DataService) { }

  ngOnInit() {
 
  }

  onSubmit(){
    const getFlightsPayload: GetFlightsPayloadData = this.composePayloadData();
    this.dataService.getFlights(getFlightsPayload).subscribe((data) => {
      this.flights = data;
    });
    if (!this.datePickerComponents.last.disabled) {
      let arrivalDate = this.datePickerComponents.last.model.year + '-';
      if (this.datePickerComponents.last.model.month < 10) {
        arrivalDate = arrivalDate + '0' + this.datePickerComponents.last.model.month + '-';
      } else {
        arrivalDate = arrivalDate + this.datePickerComponents.last.model.month + '-';
      }
      if (this.datePickerComponents.last.model.day < 10) {
        arrivalDate = arrivalDate + '0' + this.datePickerComponents.last.model.day;
      } else {
        arrivalDate = arrivalDate + this.datePickerComponents.last.model.day;
      }
      const getFlightsBackPayload: GetFlightsPayloadData = {
        departureAirportName: getFlightsPayload.arrivalAirportName,
        departureDate: arrivalDate,
        arrivalAirportName: getFlightsPayload.departureAirportName
      }
      console.log(getFlightsPayload, getFlightsBackPayload);
      this.dataService.getFlights(getFlightsBackPayload).subscribe((data) => {
        this.flightsBack = data;
      });
    }
  }

  

  composePayloadData() {
    const departureCity = this.cityPickerComponents.first.selectedCity;
    const arrivalCity = this.cityPickerComponents.last.selectedCity;
    let departureDate = this.datePickerComponents.first.model.year + '-';
    if (this.datePickerComponents.first.model.month < 10) {
      departureDate = departureDate + '0' + this.datePickerComponents.first.model.month + '-';
    } else {
      departureDate = departureDate + this.datePickerComponents.first.model.month + '-';
    }
    if (this.datePickerComponents.first.model.day < 10) {
      departureDate = departureDate + '0' + this.datePickerComponents.first.model.day;
    } else {
      departureDate = departureDate + this.datePickerComponents.first.model.day;
    }
    return {
      departureAirportName: departureCity,
      departureDate: departureDate,
      arrivalAirportName: arrivalCity
    }
  }
}
