import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private API_SERVER = "http://localhost:8080/flights";

    constructor(private httpClient: HttpClient) { }

    public getFlights(getFlightsPayload: GetFlightsPayloadData) {
        return this.httpClient.post(this.API_SERVER, getFlightsPayload);
    }
}

export type GetFlightsPayloadData = {
    departureAirportName: String;
    departureDate: String
    arrivalAirportName: String
}