import { Component, Input} from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-ngbd-datepicker-basic',
  templateUrl: './ngbd-datepicker-basic.component.html',
  styleUrls: ['./ngbd-datepicker-basic.component.css']
})
export class NgbdDatepickerBasicComponent {
  @Input() shouldDisplayDisableButton: boolean = false;

  ngOnChanges() {
    if (this.shouldDisplayDisableButton) {
      this.disabled = true;
    }
  }

  model: NgbDateStruct = {
    year: 0, month: 0, day: 0
  };

  date: {year: number, month: number} = {year: 0, month: 0};

  disabled: boolean = false;
  
  constructor(private calendar: NgbCalendar) { }

  selectToday() {
    this.model = this.calendar.getToday();
    console.log(this.shouldDisplayDisableButton);
  }

}
