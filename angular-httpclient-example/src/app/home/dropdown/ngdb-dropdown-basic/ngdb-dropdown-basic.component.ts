import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngdb-dropdown-basic',
  templateUrl: './ngdb-dropdown-basic.component.html',
  
})
export class NgdbDropdownBasicComponent {
  airportNames: String[] = ['Tel Aviv Yafo',
  'Vienna', 
 'Lisbon', 
 'Luxembourg', 
 'Milan', 
 'Athens', 
 'Brussels', 
 'London', 
 'Cluj-Napoca', 
 'Berlin',  
 'Madrid',  
 'Roma',  
 'Istanbul',  
 'Edinburgh',  
 'Bacău',  
 'Iaşi',  
    'Paris'];
  
selectedCity: String = this.airportNames[0];

  constructor() { }

  onOptionChange(event: any) {
    this.selectedCity = event.target.value;
  }

  ngOnInit(): void {
  }

}
