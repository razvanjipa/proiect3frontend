import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgdbDropdownBasicComponent } from './ngdb-dropdown-basic.component';

describe('NgdbDropdownBasicComponent', () => {
  let component: NgdbDropdownBasicComponent;
  let fixture: ComponentFixture<NgdbDropdownBasicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgdbDropdownBasicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgdbDropdownBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
